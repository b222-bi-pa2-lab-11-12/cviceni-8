#ifndef CVICENI_8_CCIRCLE_H
#define CVICENI_8_CCIRCLE_H


class CCircle {
    double x;
public:
    double getCircumference() const;
    double getContent() const;

    [[nodiscard]] double getX() const { return x; }
    void setX(double X) { x = X; }
    explicit CCircle(double x) : x(x) {}
};


#endif //CVICENI_8_CCIRCLE_H
